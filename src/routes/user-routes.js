import express from "express";
import addPlayer from '../controller/add-player.js';
import  getUser  from '../controller/get-user.js';
import  updateUser from '../controller/update-player.js';
import deletePlayer from '../controller/delete-user.js';

const router = express.Router();

router.get('/', (req, res) => {
  res.send('<h1>Welcome to users page.</h1>');
});

router.post('/new-user', addPlayer);

router.get('/:userId', getUser); 

router.put('/update-user', updateUser);

router.delete('/delete-user/:userId', deletePlayer);

export default router;
