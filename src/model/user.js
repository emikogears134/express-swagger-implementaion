import * as dbUtilities from '../utilities/firebase-db.js';

const db  = dbUtilities.default;

export const addUser = async (userData) => {
  const docRef = dbUtilities.doc(db, 'users', id);
  const { id, ...data } = userData;
    
  await dbUtilities.setDoc(docRef, data);
};

export const getUser = async (userId) => {
  const snapshot = await dbUtilities.getDoc(dbUtilities.doc(db, 'users', userId));
  if (!snapshot.exists())
    return {
      response: 400,
      message: `No document with id ${userId} found.`
    };
  const user = snapshot.data();
  
  return {
    response: 200,
    ...user
  }
};

export const deleteUser = async (userId) => {
  await dbUtilities.deleteDoc(dbUtilities.doc(db, 'users', userId));
};

export const updateUser = async (userId, userInfo, merge) => {
  const docRef = dbUtilities.doc(db, 'users', `${userId}`);

  await dbUtilities.setDoc(docRef, userInfo, { merge });
};
