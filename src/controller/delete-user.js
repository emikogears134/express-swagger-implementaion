import { deleteUser } from "../model/user.js";

const deletePlayer = async (req, res) => {
  const { userId } = req.params;

  try {
    if (!userId)
      throw 'No id provided.';

    await deleteUser(userId);
    
    res.status(200).json({
      deleted: true,
    });
  } catch (exception) {
    return res.status(400).json({
      errorMessage: typeof exception === 'string' ? exception : 'Bad request',
    });
  }
};

export default deletePlayer;