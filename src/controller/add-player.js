import { addUser } from '../model/user.js';

const addPlayer = async (req, res) => {
  const { body } = req;
  console.log('post erquiest')

  try {
    if (!body.id)
      throw 'No id provided.';
    if (!body.name) 
      throw 'No name provided.';
    if (!body.level) 
      throw 'No level provided.';
    await addUser(body);
    
    res.status(201).json({
      message: `User with id ${body.id} added.`,
    });
  } catch (exception) {
    console.log(exception)
    return res.status(400).json({
      message: typeof exception === 'string' ? message : 'an error in the db occured.'
    })
  }
};

export default addPlayer;
