import { getUser } from '../model/user.js';

const getPlayer = async (req, res) => {
  const { userId } = req.params;

  try {
    if (!userId)
      throw 'No id provided.';
    const { response, message, ...data } = await getUser(userId);
    if (response === 400)
      throw message;
    
    res.status(200).json(data);
  } catch (exception) {
    console.log(exception);
    return res.status(400).json({
      errorMessage: typeof exception === 'string' ? message : 'an error in the db occured.'
    });
  }
};

export default getPlayer;