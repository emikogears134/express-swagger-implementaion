import { updateUser } from "../model/user.js";

const updatePlayer = async (req, res) => {
  const { merge, id, ...userInfo } = req.body;
  
  try {
    if (!id) 
      throw 'No id provided';

    await updateUser(id, userInfo, merge);
    
    res.status(200).send(`Player with id ${id} updated.`);
  } catch (exception) {
    return res.status(400).json({
      error: exception,
    });
  }
};

export default updatePlayer;
