import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { doc, setDoc, getDoc, collection, addDoc, deleteDoc } from 'firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyBGHKlrZzseEi7ERsAWfTSAUjayLlyeWv4",
  authDomain: "my-very-first-project-1c2ac.firebaseapp.com",
  databaseURL: "https://my-very-first-project-1c2ac-default-rtdb.firebaseio.com",
  projectId: "my-very-first-project-1c2ac",
  storageBucket: "my-very-first-project-1c2ac.appspot.com",
  messagingSenderId: "114527806653",
  appId: "1:114527806653:web:6cc74cf7e65202d48903d4"
};

initializeApp(firebaseConfig);
const db = getFirestore();

export default db;
export {
  doc,
  setDoc,
  getDoc,
  collection,
  addDoc,
  deleteDoc
};