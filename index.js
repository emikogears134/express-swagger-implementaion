import express from 'express';
import cors from 'cors';
import user from './src/routes/user-routes.js';
import SwaggerUi from 'swagger-ui-express';
import morgan from 'morgan';
import YAML from 'yamljs';

const PORT = 3000;
const SPECS = './src/api/index.yaml';

const specs = YAML.load(SPECS);

const app = express();

app.use('/api-docs', SwaggerUi.serve, SwaggerUi.setup(specs));
app.use(express.json());
app.use(cors());
app.use(morgan('dev'));

app.use('/users', user);

app.set('port', PORT);

app.get('/', (req, res) => {
  res.send('<h1>Welcome</h1>');
});

app.listen(app.get('port'), () => {
  console.log('hey, there!');
  console.log(`In port ${app.get('port')}`);
});

